import express, { Application } from 'express';
import http from 'http';
import routes from '../routes';
import cookieParser from 'cookie-parser';

export default ({ app, server }: { app: Application, server: http.Server }) => {
    app.use(express.json({ limit: '50mb' }));
    app.use(express.urlencoded({ extended: true, limit: '50mb' }));
    app.use(cookieParser());

    app.get('/status', (req, res) => {
        res.status(200).end();
    });

    app.get('/isAlive', (req, res, next) => {
        console.log('[App is alive]');
        res.send('ALIVE');
        next();
    });

    app.use('/api', routes());
};