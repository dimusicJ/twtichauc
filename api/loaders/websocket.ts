import { Application } from 'express';
import http from 'http';
import Container from 'typedi';
import { initWebsocketHandlers } from '../websocket/handlers';
import WebsocketServer from '../websocket/websocket-server';

const websocketLoader = async ({ app, server }: { app: Application, server: http.Server }): Promise<void> => {
    const websocketServer = Container.get(WebsocketServer);
    websocketServer.start(server);

    const handlers = initWebsocketHandlers();
    Container.set('websocketHandlers', handlers);
};

export default websocketLoader;