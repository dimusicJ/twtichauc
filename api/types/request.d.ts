import { JwtTokenDataInterface } from "../interfaces/jwt-token-data";

declare module "express-serve-static-core" {
    interface Request {
        userData: JwtTokenDataInterface | undefined;
    }
}

declare module "express" {
    interface Request {
        userData: JwtTokenDataInterface | undefined;
    }
}