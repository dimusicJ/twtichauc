import Container from 'typedi';
import { IWebsocketMessageData } from '../../interfaces/websocket-message';
import DonationAlertsConnectionManager from '../../services/donation-alerts-pub-sub/donation-alerts-connection-manager';
import { RESPOND_MESSAGE_TYPES } from '../respond-message-types';
import BaseWsMessageHandler from './base-handler';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import WebsocketServer from '../websocket-server';

export default class DonationAlertsUnsubscribeHandler extends BaseWsMessageHandler {
    static type = 'DONATION_ALERTS_UNSUBSCRIBE';

    async handle(wsId: string, { channelId, username }: JwtTokenDataInterface, data?: IWebsocketMessageData) {
        const donationAlertsConnectionManager = Container.get(DonationAlertsConnectionManager);
        const websocketServer = Container.get(WebsocketServer);
        
        try {
            donationAlertsConnectionManager.disconnectUser(channelId, wsId);
            websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.DONATION_ALERTS_UNSUBSCRIBED });
        }
        catch(e) {
            websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.DONATION_ALERTS_SUBSCRIBE_ERROR });
            throw new Error(`[DONATION_ALERTS_UNSUBSCRIBE] Failed to get disconnect user ${username}`);
        }
    }
}