import Container from 'typedi';
import { IWebsocketMessageData } from '../../interfaces/websocket-message';
import UserService from '../../services/user';
import { RESPOND_MESSAGE_TYPES } from '../respond-message-types';
import BaseWsMessageHandler from './base-handler';
import TwitchPubSubManager from '../../services/twitch-pub-sub/twitchPubSubManager';
import WebsocketServer from '../websocket-server';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';

export default class ChannelPointsSubscribeHandler extends BaseWsMessageHandler {
    static type = 'CHANNEL_POINTS_SUBSCRIBE';

    async handle(wsId: string, { username }: JwtTokenDataInterface, data?: IWebsocketMessageData) {
        const userService = Container.get(UserService);
        const twitchPubSubManager = Container.get(TwitchPubSubManager);
        const websocketServer = Container.get(WebsocketServer);
        
        try {
            const user = await userService.findUser({ username });
            if (!user) {
                throw new Error(`Couldn't find user ${username}`);
            }

            await twitchPubSubManager.connectUser(user, wsId);
            websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.CP_SUBSCRIBED });
        }
        catch(e) {
            websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.CP_SUBSCRIBE_ERROR });
            throw new Error(`[CHANNEL_POINTS_SUBSCRIBE] Failed to subscribe user ${username}`);
        }
    }
}