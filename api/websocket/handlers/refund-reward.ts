import BaseWsMessageHandler from './base-handler';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import Container from 'typedi';
import TwitchRedemptionService from '../../services/twitch-pub-sub/twitch-redemption-service';
import UserService from '../../services/user';

//@TODO refactor message data property
export default class RefundHandler extends BaseWsMessageHandler {
  static type = 'REFUND_REWARD';

  async handle(userConnectionId: string, { username, channelId }: JwtTokenDataInterface, data?: any) {
    const userService = Container.get(UserService);
    const twitchRedemptionService = Container.get(TwitchRedemptionService);

    try {
      const user = await userService.findUser({ username });
      if (!user || !data) {
        throw new Error(`Couldn't find user ${username}`);
      }

      const { twitchToken: { access_token } } = user;

      await twitchRedemptionService.refundReward(access_token, channelId, data.redemptionId, data.rewardId);
    }
    catch(e) {
      throw new Error(`[REFUND_REWARD] Failed to refund`);
    }
  }
}
