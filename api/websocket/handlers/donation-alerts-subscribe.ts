import Container from 'typedi';
import { IWebsocketMessageData } from '../../interfaces/websocket-message';
import DonationAlertsConnectionManager from '../../services/donation-alerts-pub-sub/donation-alerts-connection-manager';
import UserService from '../../services/user';
import { RESPOND_MESSAGE_TYPES } from '../respond-message-types';
import BaseWsMessageHandler from './base-handler';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import WebsocketServer from '../websocket-server';

export default class DonationAlertsSubscribeHandler extends BaseWsMessageHandler {
    static type = 'DONATION_ALERTS_SUBSCRIBE';

    async handle(wsId: string, { username }: JwtTokenDataInterface, data?: IWebsocketMessageData) {
        const userService = Container.get(UserService);
        const donationAlertsConnectionManager = Container.get(DonationAlertsConnectionManager);
        const websocketServer = Container.get(WebsocketServer);

        try {
            const user = await userService.findUser({ username });
            if (!user) {
                throw new Error(`Couldn't find user ${username}`);
            }

            await donationAlertsConnectionManager.connectUser(user, wsId);
            console.log(`send response ${wsId}`);
            websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.DONATION_ALERTS_SUBSCRIBED });
        }
        catch(e) {
            websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.DONATION_ALERTS_SUBSCRIBE_ERROR });
            throw new Error(`Failed to get user ${username}`);
        }
    }
}