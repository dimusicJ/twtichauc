import { IWebsocketMessageData } from '../../interfaces/websocket-message';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';

export default class BaseWsMessageHandler {
    constructor() {
    }

    handle(userConnectionId: string, userData: JwtTokenDataInterface, data?: IWebsocketMessageData): void {
        throw new Error('Not implemented');
    }
}