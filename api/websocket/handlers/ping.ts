import { IWebsocketMessageData } from "../../interfaces/websocket-message";
import BaseWsMessageHandler from "./base-handler";
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';

export default class PingHandler extends BaseWsMessageHandler {
    static type = 'PING';

    async handle(userConnectionId: string, userData: JwtTokenDataInterface, data?: IWebsocketMessageData) {
        console.log('ping', userConnectionId);
    }
}