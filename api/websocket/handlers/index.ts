import { IWebsocketMessage } from '../../interfaces/websocket-message';
import IdentifyClientHandler from './identify-client';
import MockPurchaseHandler from './mock-purchase';
import ChannelPointsSubscribeHandler from './channel-points-subsribe';
import ChannelPointsUnsubscribeHandler from './channel-points-unsubscribe';
import PingHandler from './ping';
import BaseWsMessageHandler from './base-handler';
import DonationAlertsSubscribeHandler from './donation-alerts-subscribe';
import DonationAlertsUnsubscribeHandler from './donation-alerts-unsubscribe';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import RefundHandler from './refund-reward';

interface WebsocketHandlersInterface {
    [k: string]: BaseWsMessageHandler;
}

export class WebsocketHandlers {
    handlers: WebsocketHandlersInterface;

    constructor(handlers: WebsocketHandlersInterface) {
        this.handlers = handlers;
    }

    async handle(userConnectionId: string, userData: JwtTokenDataInterface, message: IWebsocketMessage) {
        if (!this.handlers[message.type]) {
            throw new Error(`Couldn't find handler for message type "${message.type}"`);
        }

        return await this.handlers[message.type].handle(userConnectionId, userData, message.data);
    }
}

const initWebsocketHandlers = () => {
    return new WebsocketHandlers({
        [PingHandler.type]: new PingHandler(),
        [IdentifyClientHandler.type]: new IdentifyClientHandler(),
        [MockPurchaseHandler.type]: new MockPurchaseHandler(),
        [ChannelPointsSubscribeHandler.type]: new ChannelPointsSubscribeHandler(),
        [ChannelPointsUnsubscribeHandler.type]: new ChannelPointsUnsubscribeHandler(),
        [DonationAlertsSubscribeHandler.type]: new DonationAlertsSubscribeHandler(),
        [DonationAlertsUnsubscribeHandler.type]: new DonationAlertsUnsubscribeHandler(),
        [RefundHandler.type]: new RefundHandler(),
    });
}

export { initWebsocketHandlers };