import { IWebsocketMessageData } from '../../interfaces/websocket-message';
import ChannelPointsService from '../../services/channel-points';
import BaseWsMessageHandler from './base-handler';
import { JwtTokenDataInterface } from '../../interfaces/jwt-token-data';
import Container from 'typedi';
import WebsocketServer from '../websocket-server';
import { RESPOND_MESSAGE_TYPES } from '../respond-message-types';

export default class MockAuctionPurchaseHandler extends BaseWsMessageHandler {
    static type = 'GET_MOCK_DATA';

    async handle(wsId: string, userData: JwtTokenDataInterface, data?: IWebsocketMessageData) {
        const mockPurchase = ChannelPointsService.createMockPurchase();
        const websocketServer = Container.get(WebsocketServer);

        websocketServer.sendMessageByClientsId([wsId], { type: RESPOND_MESSAGE_TYPES.PURCHASE, purchase: mockPurchase });
    }
}