import http from 'http';
import jwt from 'jsonwebtoken';
import net from 'net';
import Container, { Inject, Service } from 'typedi';
import { v4 as uuidv4 } from 'uuid';
import WebSocket, { AddressInfo } from 'ws';
import config from '../config';
import { IAucWebsocket, IAucWebsocketServerRequest } from '../interfaces/auc-websocket';
import { IWebsocketMessage } from '../interfaces/websocket-message';
import { WebsocketHandlers } from './handlers';
import User from '../models/user';
import DonationAlertsConnectionManager from '../services/donation-alerts-pub-sub/donation-alerts-connection-manager';
import TwitchPubSubManager from '../services/twitch-pub-sub/twitchPubSubManager';
import TelegramBotService from '../services/telegram-bot';
import url from 'url';

@Service()
export default class WebsocketServer {
    wsServer?: WebSocket.Server;
    clientsMap: Map<string, WebSocket>;

    constructor(
      @Inject('userModel') private userModel: typeof User,
      private telegramBotService: TelegramBotService,
    ) {
        this.clientsMap = new Map<string, WebSocket>();
    }

    async start(server: http.Server) {
        this.wsServer = new WebSocket.Server({ noServer: true });

        server.on('upgrade',  this.handleUpgrade.bind(this));

        this.wsServer.on('listening', this.handleListening.bind(this));

        this.wsServer.on('connection', this.handleConnection.bind(this));

        this.wsServer.on('close', this.handleClose.bind(this));
    }

    sendMessageByClientsId = (ids: string[], message: IWebsocketMessage): void => {
        if (!this.wsServer) {
            throw new Error(`ws server is not running`);
        }

        ids.forEach((id) => {
            const clientSocket = this.clientsMap.get(id);

            if (clientSocket && clientSocket.readyState === clientSocket.OPEN) {
                clientSocket.send(JSON.stringify(message));
            }
        });
    }

    handleUpgrade(request: IAucWebsocketServerRequest, socket: net.Socket, head: Buffer) {
        //let token = (request.headers['x-access-token'] || request.headers['authorization']) as string;
        const token = url.parse(request.url || '', true).query.jwtToken as string;

        jwt.verify(token, config.JWT_SECRET, (err: any, decodedToken: any) => {
            if (err) {
                socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
                socket.destroy();
                return;
            } else {
                this.wsServer!.handleUpgrade(request, socket, head, (ws) => {
                    request.userData = {
                        ...decodedToken,
                    };

                    this.wsServer!.emit('connection', ws, request);
                });
            }
        });
    }

    handleListening() {
        console.log(
            `WebSocket Server is now listening on PORT: ${(this.wsServer?.address() as AddressInfo).port}`
        )
    }

    disconnectPubSub = async (channelId: string, wsId?: string): Promise<void> => {
        const user = await this.userModel.findOne({ channelId }).exec();

        if (user && wsId) {
            const twitchPubSubManager = Container.get(TwitchPubSubManager);
            const donationAlertsConnectionManager = Container.get(DonationAlertsConnectionManager);

            await Promise.all([
                await twitchPubSubManager.disconnectUser(user, wsId),
                await donationAlertsConnectionManager.disconnectUser(user.channelId, wsId),
            ]);
        }
    }

    async handleConnection(ws: IAucWebsocket, req: IAucWebsocketServerRequest) {
        if (!req.userData) {
            console.error(`[WS handleConnection] Failed to get "userData" param on the request`);
            ws.terminate();
            return;
        }

        const { username } = req.userData;

        ws.isAlive = true;
        ws.id = `${username}.${uuidv4()}`;
        ws.userData = req.userData;

        const checkConnectionInterval = setInterval(async () => {
            if (!ws.isAlive) {
                console.log(`terminate ws ${ws.id}; user: ${ws.userData.username}`);
                ws.terminate();
                
                clearInterval(checkConnectionInterval);
                this.clientsMap.delete(ws.id || '');

                await this.disconnectPubSub(ws.userData.channelId, ws.id);

                return;
            }
            ws.isAlive = false;
            ws.ping();
        }, 10000);

        ws.on('pong', () => {
            console.log('~pong');
            ws.isAlive = true;
        });

        ws.on('message', this.handleMessage.bind(this, ws));

        this.clientsMap.set(ws.id, ws);

        await this.telegramBotService.logConnectedUser(req.userData.username);
    }

    async handleMessage(ws: IAucWebsocket, data: WebSocket.Data) {
        if (!ws.id) {
            console.error(`Couldn't find ws.id`);
            return;
        }

        const message = JSON.parse(data as string);
        //@TODO update frontend; remove
        const { type, ...restMessage } = message;
        const newMessage = {
            type,
            data: {
                ...restMessage,
            },
        };

        const handlers = Container.get('websocketHandlers') as WebsocketHandlers;

        await handlers.handle(ws.id, ws.userData, newMessage);
    }

    handleClose = async ({ id, userData }: IAucWebsocket) => {
        if (id) {
            this.clientsMap.delete(id);

            await this.disconnectPubSub(userData.channelId, id);

            console.log(`disconnected ${id}`);
        }
    }
}