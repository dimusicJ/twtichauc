if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

import 'reflect-metadata';
import express from 'express';
import http from 'http';
import config from './config';
import loaders from './loaders';

// @ts-ignore
if(!global.XMLHttpRequest) global.XMLHttpRequest = require("xhr2");
// @ts-ignore
if(!global.WebSocket) global.WebSocket = require("ws");

async function startServer() {
    const app = express();
    
    const server = http.createServer(app);
    
    await loaders({ app, server });

    server.listen(config.PORT, () => {
        console.log(`app listening on port ${config.PORT}`);
    }).on('error', err => {
        console.error(err);
        process.exit(1);
    });
}

startServer();
