import { Router } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import UserService from '../services/user';
import TwitchRedemptionService from '../services/twitch-pub-sub/twitch-redemption-service';

export default (app: Router) => {
    const userService = Container.get(UserService);
    const twitchRedemptionService = Container.get(TwitchRedemptionService);

    app.post('/settings', middlewares.TokenAuthMiddleware, async (req, res, next) => {
        await userService.updateAucSettings(res.locals.userData!.username, req.body);
      
        res.send();
        next();
    });

    app.post('/integration', middlewares.TokenAuthMiddleware, async (req, res, next) => {
        const user = await userService.updateIntegration(res.locals.userData!.username, req.body);

        if (req.body.twitch.rewards && user) {
            const {
                integration: {
                    twitch: { rewards = [], rewardsPrefix = '' },
                },
                twitchToken: { access_token },
                channelId,
            } = user;

            await twitchRedemptionService.refreshRewards(rewards, rewardsPrefix, access_token, channelId);
        }

        res.send();
        next();
    });
}