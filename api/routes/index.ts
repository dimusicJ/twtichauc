import { Router } from 'express';
import settings from './settings';
import twitch from './twitch';
import user from './user';
import donationAlerts from './donation-alerts';

export default () => {
    const app = Router();

    user(app);
    settings(app);
    twitch(app);
    donationAlerts(app);

    return app;
}