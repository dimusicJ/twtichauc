import { Router } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import { DonationAlertsAuthService } from '../services/donation-alerts-auth';
import UserService from '../services/user';

const route = Router();

export default (app: Router) => {
    app.use('/donationalerts', route);

    const userService = Container.get(UserService);
    const donationAlertsAuthService = Container.get(DonationAlertsAuthService);

    route.post('/auth', middlewares.TokenAuthMiddleware, async (req, res, next) => {
        try {
            await donationAlertsAuthService.authenticate(
              res.locals.userData.channelId,
              req.body.code,
              req.body.redirectUri,
            );
            res.send('');
            next();
        }
        catch(e) {
            console.error(e);
            return next(new Error('Donation Alerts authentication failed'));
        }
    });

    route.get('/user', middlewares.TokenAuthMiddleware, async (req, res, next) => {
        const user = await userService.findUser({ username: res.locals.userData.username });
        const daUser = await donationAlertsAuthService.getUser(user!.donationAlerts!.token!.access_token);
        res.send(daUser);
        next();
    })
}