import { Router } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import UserService from '../services/user';

const route = Router();

export default (app: Router) => {
    app.use('/user', route);
    
    route.get('/userData', middlewares.TokenAuthMiddleware, async(req, res, next) => {
        const userService = Container.get(UserService);

        const user = await userService.findUser({ username: res.locals.userData!.username });

        if (user) {
            const { username, settings, integration, daToken } = user;

            res.send({ username, settings, integration, hasDAAuth: !!daToken?.access_token });
        }
        next();
    });
}
