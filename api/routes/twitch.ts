import { Router, Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { Container } from 'typedi';
import config from '../config';
import TwitchAuthService from '../services/twitch-auth';

const route = Router();

export default (app: Router) => {
    app.use('/twitch', route);

    route.post('/auth', async (req: Request, res: Response, next: NextFunction) => {
        try {
            const twitchAuthService = Container.get(TwitchAuthService);
            const userData = await twitchAuthService.signIn(req.body.code);

            let token = jwt.sign(userData, config.JWT_SECRET, {
                expiresIn: 1000 * 60 * 60 * 24 * 30 * 12,
            });

            res.cookie('jwtToken', token);

            console.log(token);
            console.log('userData:', userData);
            
            res.json({
                jwtToken: token,
            });

            next();
        }
        catch(e) {
            return next(new Error('Authentication failed'));
        }
    });
}