export interface IWebsocketMessageData {
    [k: string]: string;
}

export interface IWebsocketMessage {
    type: string,
    data?: IWebsocketMessageData;
    //@TODO Remove; Update frontend
    purchase?: any;
}