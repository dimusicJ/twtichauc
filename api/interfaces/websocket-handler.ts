import { EventEmitter } from "events";
import { IWebsocketMessageData } from "./websocket-message";

export interface WebsocketHandlerInterface {
    wsServerEventEmitter: EventEmitter;
    
    handle: (messageData?: IWebsocketMessageData) => void;
}