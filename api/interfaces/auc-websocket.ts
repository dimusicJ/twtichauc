import { EventEmitter } from "events";
import { IncomingMessage } from "http";
import WebSocket from "ws";
import { JwtTokenDataInterface } from "./jwt-token-data";

export interface IAucWebsocketServerRequest extends IncomingMessage {
    userData?: JwtTokenDataInterface;
}

export interface WebsocketServerPubSubEmitterInterface extends EventEmitter {
    id?: string;
}

export interface IAucWebsocket extends WebSocket {
    isAlive: boolean;
    id?: string;
    userData: JwtTokenDataInterface;
    pubSubEmitter: WebsocketServerPubSubEmitterInterface;
}
