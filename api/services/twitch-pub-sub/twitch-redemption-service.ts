import { Service } from 'typedi';
import { Key } from '../../types/common';
import { ENDPOINTS, REDEMPTION_STATUS } from '../../config/constants';
import axios, { AxiosRequestConfig } from 'axios';
import CONFIG from '../../config';
import { RewardSetting } from '../../models/user';
import { ITwitchPubSubRedemption } from './messages/points-redemption';

export interface IReward {
  id: string;
  title: string;
  cost: number;
  'broadcaster_id': string;
  'background_color': string;
  'is_user_input_required': boolean;
}

export interface IRewardsResponse {
  data: IReward[];
}

const getCustomRewardsConfig = (accessToken: string, channelId: Key): AxiosRequestConfig => ({
  params: {
    broadcaster_id: channelId,
  },
  headers: {
    'Client-ID': CONFIG.TWITCH_CLIENT_ID,
    'Authorization': `Bearer ${accessToken}`,
  }
});

@Service()
class TwitchRedemptionService {
  private openedRewards: Map<Key, string[]>;

  constructor() {
    this.openedRewards = new Map<string, string[]>();
  }

  openReward = async ({ cost, color }: RewardSetting, title: string, config: AxiosRequestConfig): Promise<string> => {
    const reward = {
      title: `${title} ${cost}`,
      cost,
      background_color: color,
      is_user_input_required: true,
    };

    const { data: { data } } = await axios.post<IRewardsResponse>(ENDPOINTS.CUSTOM_REWARDS, reward, config);

    return data[0].id;
  }

  openRewards = async (rewards: RewardSetting[], title: string, token: string, channelId: Key): Promise<void> => {
    try {
      const config = getCustomRewardsConfig(token, channelId);

      const createdRewards = await Promise.all(
        rewards.map((reward) => this.openReward(reward, title, config)),
      );

      this.openedRewards.set(channelId, createdRewards);
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  }

  closeReward = async (id: string, config: AxiosRequestConfig): Promise<void> => {
    await axios.delete(ENDPOINTS.CUSTOM_REWARDS, { ...config, params: { ...config.params, id } });
  }

  closeRewards = async (token: string, channelId: Key): Promise<void> => {
    const config = getCustomRewardsConfig(token, channelId);
    const rewards = this.openedRewards.get(channelId);

    if (rewards) {
      await Promise.all(
        rewards.map((id) => this.closeReward(id, config)),
      );
    }

    this.openedRewards.delete(channelId);
  }

  updateCreatedRewards = async (token: string, channelId: Key): Promise<void> => {
    const config = getCustomRewardsConfig(token, channelId);
    config.params['only_manageable_rewards'] = true;

    const { data: { data } } = await axios.get<IRewardsResponse>(ENDPOINTS.CUSTOM_REWARDS, config);

    this.openedRewards.set(channelId, data.map(({ id }) => id));
  }

  refundReward = async (token: string, channelId: Key, redemptionId: string, rewardId: string) => {
    const config = getCustomRewardsConfig(token, channelId);
    config.params.id = redemptionId;
    config.params['reward_id'] = rewardId;

    await axios.patch(ENDPOINTS.CUSTOM_REDEMPTIONS, { status: REDEMPTION_STATUS.CANCELED }, config).catch((err) => console.log(err));
  }

  isAucRedemption = (channelId: Key, redemption: ITwitchPubSubRedemption): boolean => {
    return Boolean(this.openedRewards.get(channelId)?.includes(redemption.reward.id));
  }

  refreshRewards = async (rewards: RewardSetting[], title: string, token: string, channelId: Key): Promise<void> => {
    if (this.openedRewards.has(channelId)) {
      await this.closeRewards(token, channelId);
      await this.openRewards(rewards, title, token, channelId);
    }
  }
}

export default TwitchRedemptionService;
