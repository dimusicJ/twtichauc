export enum REQUEST_MESSAGE_TYPE {
    LISTEN = 'LISTEN',
    UNLISTEN = 'UNLISTEN',
    PING = 'PING',
}

export interface TwitchPubSubResponseTypeMessageInterface {
    type: 'RESPONSE';
	  error: string;
    nonce?: string;
}

export interface TwitchPubSubResponseMessageTypeInterface {
    type: 'MESSAGE';
    data: {
        topic: string;
        message: string;
    };
}

export interface TwitchPubSubRequestMessageInterface {
    type: REQUEST_MESSAGE_TYPE,
    data?: {
        [k: string]: string;
    };
}
