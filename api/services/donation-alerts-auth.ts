import axios from 'axios';
import { Inject, Service } from 'typedi';
import config from '../config';
import { IDaUserData } from '../interfaces/donation-alerts';
import User, { IUserDonationAlertsToken } from '../models/user';
import UserService from './user';

const TOKEN_URL = 'https://www.donationalerts.com/oauth/token';
const REFRESH_TOKEN_URL = TOKEN_URL;

@Service()
class DonationAlertsAuthService {
    constructor(
        @Inject('userModel') private userModel: typeof User,
        private userService: UserService,
    ) {
    }

    async authenticate(channelId: string, code: string, redirectUri: string): Promise<void> {
        try {
            const daToken = await this.createToken(code, redirectUri);
            await this.userService.updateDonationAlertsToken(channelId, daToken);
        }
        catch(e) {
            console.error('Failed to authenticate Donation Alerts', e);
        }
    }

    createToken = async (code: string, redirectUri: string): Promise<IUserDonationAlertsToken> => {
        try {
            const body = {
                grant_type: 'authorization_code',
                client_id: config.DONATIONALERTS_CLIENT_ID,
                client_secret: config.DONATIONALERTS_CLIENT_SECRET,
                redirect_uri: redirectUri,
                code,
            };

            const { data } = await axios.post<IUserDonationAlertsToken>(TOKEN_URL, body);

            return data;
        }
        catch (e) {
            if (e.response) {
                throw new Error(e.response.data.message);
            }
            else {
                throw e;
            }
        }
    }

    async refreshToken(channelId: string): Promise<void> {
        try {
            const user = await this.userService.findUser({ channelId });
            const daRefreshToken = user!.daToken?.refresh_token;

            if (!daRefreshToken) {
                throw new Error('Donation Alerts refresh token not found.');
            }

            const body = {
                grant_type: 'refresh_token',
                refresh_token: daRefreshToken,
                client_id: config.DONATIONALERTS_CLIENT_ID,
                client_secret: config.DONATIONALERTS_CLIENT_SECRET,
            };

            const { data } = await axios.post(REFRESH_TOKEN_URL, body);
            
            await this.userService.updateDonationAlertsToken(channelId, data);

            console.log('donation alerts token was refreshed');
        }
        catch (e) {
            if (e.response) {
                const responseData = e.response.data;
                if (responseData.error) {
                    throw new Error(responseData.message);
                }
            }
            else {
                throw e;
            }
        }
    }

    validateToken = async (channelId: string): Promise<boolean> => {
        const { daToken } = await this.userModel.findOne({ channelId }).exec() || {};
        const { access_token } = daToken || {};

        try {
            return !access_token || !!(await this.getUser(access_token));
        } catch (e) {
            return false;
        }
    }

    async getUser(token: string): Promise<IDaUserData> {
        try {
            const { data: { data } } = await axios.get<{ data: IDaUserData }>('https://www.donationalerts.com/api/v1/user/oauth', {
                headers: { Authorization: `Bearer ${token}` },
            });

            return data;
        }
        catch (e) {
            console.error(e.toJSON());
            throw e;
        }
    }
}

export { DonationAlertsAuthService };
