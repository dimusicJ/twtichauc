import randomColor from 'randomcolor';
import { getRandomIntInclusive } from '../utils/number';
import { ITwitchPubSubRedemption } from './twitch-pub-sub/messages/points-redemption';
import { IPurchase } from '../types/common';

let colorsMap = randomColor({ count: 5, luminosity: 'dark' });

export default class ChannelPointsService {
    constructor() {
    }

    static getPurchaseFromReward = ({ id, user, reward, user_input, redeemed_at }: ITwitchPubSubRedemption): IPurchase => ({
        id,
        rewardId: reward.id,
        username: user.display_name,
        cost: Number(reward.cost),
        color: reward.background_color,
        message: user_input,
        timestamp: redeemed_at,
    })

    static createMockPurchase = () => {
        const randomValue = getRandomIntInclusive(1, 5);
        return {
            timestamp: new Date().toISOString(),
            cost: randomValue * 1000,
            username: 'username',
            message: `messageMock.${randomValue}`,
            color: colorsMap[randomValue - 1],
            id: Math.random(),
        };
    }
}