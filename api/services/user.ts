import { Document } from 'mongoose';
import { Inject, Service } from 'typedi';
import { IDaUserData } from '../interfaces/donation-alerts';
import User, {
    IUser,
    IUserAucSettings,
    IUserDonationAlertsToken,
    IUserIntegration,
    UserTwitchTokenInterface
} from '../models/user';
import { objectToDotNotation } from '../utils/object';

interface UserData {
    id: number;
    username: string;
}

@Service()
export default class UserService {
    constructor(
        @Inject('userModel') private userModel: typeof User
    ) {

    }

    async createUser(userData: any) {
        await this.userModel.create({ ...userData, aucSettings: {} });
    }

    async updateTwitchToken(channelId: string, twitchToken: UserTwitchTokenInterface) {
        await this.userModel.updateOne({ channelId }, { twitchToken });
    }

    async updateAucSettings(username: string, settings: IUserAucSettings) {
        await this.userModel.updateOne({ username }, { $set: objectToDotNotation({ settings }) });
    }

    async updateIntegration(username: string, integration: IUserIntegration) {
        return this.userModel.findOneAndUpdate({ username }, { $set: objectToDotNotation({ integration }) }, { new: true });
    }

    async updateDonationAlertsToken(channelId: string, daToken: IUserDonationAlertsToken) {
        await this.userModel.updateOne({ channelId }, { daToken });
    }

    async updateDonationalertsInfo(username: string, daToken: IUserDonationAlertsToken, daUser: IDaUserData) {
        await this.userModel.updateOne({ username }, {
            donationAlerts: {
                token: daToken,
                userId: daUser.id,
                userCode: daUser.code,
                userName: daUser.name,
                socketConnectionToken: daUser.socket_connection_token,
            }
        });
    }

    async updateDonationalertsClientId(username: string, clientId: string | number) {
        await this.userModel.updateOne({ username }, {
          $set: {
            "donationAlerts.clientId": clientId,
          },
        });
    }

    async updateDonationalertsNewDonationsChannel(username: string, channelToken: string) {
        await this.userModel.updateOne({ username }, {
            $set: {
              "donationAlerts.newDonationsChannelToken": channelToken,
            },
        });
    }

    async isUserRegistered(username: string) {
        return await this.userModel.exists({ username });
    }

    async findUser(userProps: any): Promise<IUser & Document | null> {
        return await this.userModel.findOne(userProps).exec();
    }

    async getUserData(userProps: any): Promise<UserData | null> {
        const user = await this.findUser(userProps);
        if (!user) {
            return null;
        }

        return {
            id: user.id,
            username: user.username,
        };
    }

    async getUserSettings(userProps: any): Promise<Pick<IUser, 'integration' | 'settings'> | null> {
        const user = await this.findUser(userProps);
        if (!user) {
            return null;
        }

        return {
            integration: user.integration,
            settings: user.settings,
        };
    }
}