import { Service } from 'typedi';
import WebSocket from 'ws';
import Centrifuge from 'centrifuge';
import { IDonationAlertsCentrifugeDonationMessage } from './messages/donation-alerts-donation';

interface DonationAlertsSubscribeCallbacksInterface {
    onDonation?: (message: IDonationAlertsCentrifugeDonationMessage) => void;
}

const DA_CENTRIFUGE_URL = 'wss://centrifugo.donationalerts.com/connection/websocket';
const DA_SUBSCRIBE_URL = 'https://www.donationalerts.com/api/v1/centrifuge/subscribe';
const RECONNECTION_DELAY_MIN = 1000;
const RECONNECTION_DELAY_MAX = 5000;

//@TODO Handle token refresh
@Service()
export default class DonationAlertsCentrifugeService {
    private centrifugeClient?: Centrifuge;
    
    constructor(
        private daSocketConnectionToken: string,
        private subscribeCallbacks: DonationAlertsSubscribeCallbacksInterface,
    ) {
        
    }

    async connect(accessToken: string, userId: number) {
        this.closeConnection();

        return new Promise((resolve, reject) => {
            this.centrifugeClient = new Centrifuge(DA_CENTRIFUGE_URL, {
                subscribeEndpoint: DA_SUBSCRIBE_URL,
                subscribeHeaders: {
                    Authorization: `Bearer ${accessToken}`
                },
                minRetry: RECONNECTION_DELAY_MIN,
                maxRetry: RECONNECTION_DELAY_MAX,
                debug: true,
                websocket: WebSocket,
            });

            this.centrifugeClient.setToken(this.daSocketConnectionToken);

            this.centrifugeClient.on('connect', (e) => {
                console.log('[DonationAlertsCentrifugeService] connect', e);
            });
            this.centrifugeClient.on('disconnect', (e) => {
                console.log('[DonationAlertsCentrifugeService] disconnect', e);
                reject(e);
            });
            this.centrifugeClient.on('error', (err) => {
                console.error('[DonationAlertsCentrifugeService] error', err);
                reject(err);
            });

            this.centrifugeClient.connect();

            const subCallbacks = {
                publish: (message: any) => {
                    const messageType = message && message.data && message.data.message_type;
                    if (messageType && this.subscribeCallbacks.onDonation) {
                        this.subscribeCallbacks.onDonation(message as IDonationAlertsCentrifugeDonationMessage);
                    }
                },
                join: (message: unknown) => {
                    console.log('[DonationAlertsCentrifugeService] channel join', message);
                },
                subscribe: (message: unknown) => {
                    console.log('[DonationAlertsCentrifugeService] channel subscribe', message);
                    resolve(true);
                },
                error: (err: unknown) => {
                    console.log('[DonationAlertsCentrifugeService] channel subscribe error', err);
                    reject(err);
                },
                unsubscribe: (context: unknown) => {
                    console.log('[DonationAlertsCentrifugeService] channel unsubscribe', context);
                },
            };

            this.centrifugeClient.subscribe(`$alerts:donation_${userId}`, subCallbacks);
        });
    }

    closeConnection() {
        this.centrifugeClient?.disconnect();
    }
}