import { Inject, Service } from 'typedi';
import User, { IUser } from '../../models/user';
import WebsocketServer from '../../websocket/websocket-server';
import { IDonationAlertsCentrifugeDonationMessage, IDonationAlertsDonation } from './messages/donation-alerts-donation';
import { DonationAlertsAuthService } from '../donation-alerts-auth';
import DonationAlertsCentrifugeService from './donation-alerts-centrifuge';
import { IPurchase } from '../../types/common';
import PubSubManager, { AddUserStateEnum, RemoveUserStateEnum } from '../pub-sub-manager';

@Service()
export default class DonationAlertsConnectionManager extends PubSubManager {
    userConnections: Map<string, DonationAlertsCentrifugeService> = new Map();

    constructor(
      @Inject('userModel') private userModel: typeof User,
      @Inject(() => WebsocketServer) private websocketServer: WebsocketServer,
      private donationAlertsAuthService: DonationAlertsAuthService,
    ) {
      super('DonationAlertsConnectionManagerService');
    }

    connectUser = async ({ channelId, daToken }: IUser, wsId: string): Promise<void> => {
        this.log(`connecting user: ${channelId}`);
        const { access_token } = daToken || {};

        if (!access_token) {
            throw new Error(`Couldn't find Donation Alerts access token. Make sure Donation Alerts user is authorized.`);
        }

        const addState = this.addUser(channelId, wsId);

        if (addState === AddUserStateEnum.First) {
            await this.addCentrifugeConnection(access_token, channelId);
        }
    }

    addCentrifugeConnection = async (token: string, channelId: string): Promise<void> => {
        const { socket_connection_token, id } = await this.donationAlertsAuthService.getUser(token);
        console.log(await this.donationAlertsAuthService.getUser(token));

        const donationAlertsCentrifugeServiceInstance = new DonationAlertsCentrifugeService(
          socket_connection_token,
          {
              onDonation: this.handleDonation.bind(this, channelId),
          },
        );

        try {
            await donationAlertsCentrifugeServiceInstance.connect(token, id);
            this.userConnections.set(channelId, donationAlertsCentrifugeServiceInstance);
        }
        catch(e) {
            this.log(`Failed to connect channel ${channelId}`);
            console.log(e);
            throw e;
        }
    }

    disconnectUser = (channelId: string, wsId: string): void => {
        const removeState = this.removeUser(channelId, wsId);

        if (removeState === RemoveUserStateEnum.Last) {
            this.userConnections.get(channelId)!.closeConnection();
            this.userConnections.delete(channelId);
        }
    }

    async handleDonation(channelId: string, { data }: IDonationAlertsCentrifugeDonationMessage) {
        const ids = this.clients.get(channelId) || [];
        const purchase = this.getPurchase(data);

        this.websocketServer.sendMessageByClientsId(ids, { type: 'PURCHASE', purchase });
    }

    // @TODO Refactor to use AuctionRewardService
    getPurchase = ({ id, username, message, created_at, amount_in_user_currency }: IDonationAlertsDonation): IPurchase => ({
        id: id.toString(),
        username,
        message,
        timestamp: created_at,
        cost: Math.round(amount_in_user_currency),
        color: '#000000',
        isDonation: true,
    })
}