class Logger {
  constructor(private readonly serviceName: string) {}

  log = (message: string): void => console.log(`[${this.serviceName}]: ${message}`);
}

export default Logger;
