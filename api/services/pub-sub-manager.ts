import { Key } from "../types/common";
import Logger from './logger';

export enum AddUserStateEnum {
  Exist,
  First,
  Added,
}

export enum RemoveUserStateEnum {
  NotExist,
  Last,
  Removed,
}

class PubSubManager extends Logger {
  protected clients: Map<Key, string[]>;

  constructor(serviceName: string) {
    super(serviceName);

    this.clients = new Map<string, string[]>();
  }


  addUser = (channelId: Key, id: string): AddUserStateEnum => {
    const activeUsers = this.clients.get(channelId);

    if (!activeUsers) {
      this.clients.set(channelId, [id]);

      return AddUserStateEnum.First;
    } else if (!activeUsers.includes(id)) {
      activeUsers.push(id);

      return AddUserStateEnum.Added;
    }

    return AddUserStateEnum.Exist;
  }

  removeUser = (channelId: Key, id: string): RemoveUserStateEnum => {
    const activeUsers = this.clients.get(channelId);

    if (activeUsers?.length === 1) {
      this.clients.delete(channelId);

      return RemoveUserStateEnum.Last
    } else if (activeUsers?.includes(id)) {
      const removeIndex = activeUsers.findIndex((client) => client === id);

      activeUsers.splice(removeIndex, 1);

      return RemoveUserStateEnum.Removed;
    }

    return RemoveUserStateEnum.NotExist;
  }
}

export default PubSubManager;
