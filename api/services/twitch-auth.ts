import { Inject, Service } from 'typedi';
import axios from 'axios';
import config from '../config';
import User, { UserTwitchTokenInterface } from '../models/user';
import UserService from './user';
import { DEFAULT_USER } from '../types/common';

interface ITwitchUserInfoResponse {
    token: {
        user_name: string;
        user_id: string;
    };
}

const clientParams = {
    client_id: config.TWITCH_CLIENT_ID,
    client_secret: config.TWITCH_CLIENT_SECRET,
}

@Service()
class TwitchAuthService {
    constructor(
        @Inject('userModel') private userModel: typeof User,
        private userService: UserService,
    ) {
    }

    public async signIn(code: string) {
        try {
            const twitchToken = await this.getTwitchToken(code);
            const {
                token: {
                    user_name: username,
                    user_id: channelId,
                },
            } = await this.getUserInfo(twitchToken.access_token);
            const isRegistered = await this.userModel.exists({ channelId });
            const userData = { username, channelId };

            isRegistered
                ? await this.userModel.updateOne({ username }, { twitchToken })
                : await this.userModel.create({
                    ...userData,
                    twitchToken,
                    ...DEFAULT_USER,
                });
            
            return userData;
        }
        catch(e) {
            console.error(e);
            throw new Error('Failed to sign in');
        }
    }

    public async getUserInfo(token: string): Promise<ITwitchUserInfoResponse> {
        try {
            const { data } = await axios.get('https://api.twitch.tv/kraken', {
                headers: {
                    'Accept': 'application/vnd.twitchtv.v5+json',
                    'Client-ID': config.TWITCH_CLIENT_ID,
                    'Authorization': `OAuth ${token}`,
                }
            });
    
            return data;
        }
        catch(e) {
            console.error(e);
            throw e;
        }
    }

    public async getTwitchToken(code: string): Promise<UserTwitchTokenInterface> {
        try {
            const { data } = await axios.post('https://id.twitch.tv/oauth2/token', {}, {
                params: {
                    ...clientParams,
                    redirect_uri: config.TWITCH_REDIRECT_URL,
                    grant_type: 'authorization_code',
                    code,
                }
            });
    
            return data;
        }
        catch(e) {
            console.error(e);
            throw e;
        }
    }

    public async refreshToken(channelId: string) {
        const user = await this.userService.findUser({ channelId });
        if (!user) {
            throw new Error(`Couldn't find user ${channelId}`);
        }

        const { twitchToken: { refresh_token } } = user;

        const { data } = await axios.post('https://id.twitch.tv/oauth2/token', {}, {
            params: {
                ...clientParams,
                grant_type: 'refresh_token',
                refresh_token,
            },
        });

        await this.userService.updateTwitchToken(channelId, data);
        
        console.log('twitch token was refreshed');
    }

    validateToken = async (channelId: string): Promise<boolean> => {
        const user = await this.userModel.findOne({ channelId }).exec();

        if (!user) {
            return false;
        }

        const { twitchToken: { access_token } } = user;

        try {
            await axios.get('https://id.twitch.tv/oauth2/validate', { headers: { Authorization: `OAuth ${access_token}` } });

            return true;
        } catch (e) {
            return false;
        }
    }
}

export default TwitchAuthService;